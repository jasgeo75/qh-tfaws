FROM registry.gitlab.com/jasgeo75/qh-tfbase

RUN set -eux; \
  apt-get update \
  && apt-get install -y \
    python3-pip \
  ;

#=============================================================================#
#    Install AWS CLI
#=============================================================================#
RUN set -eux; \
  pip3 install \
    awscli; \
    aws --version

#=============================================================================#
#    Install the AWS IAM Authenticator for Kubernetes
#=============================================================================#
RUN set -eux; \
  curl -fsSL https://api.github.com/repos/kubernetes-sigs/aws-iam-authenticator/releases/latest | \
    jq -r '.assets[]|.browser_download_url|select(contains("linux_amd64"))' | \
    xargs -n1 curl -fsSL -o /usr/local/bin/aws-iam-authenticator; \
  chmod +x /usr/local/bin/aws-iam-authenticator; \
  aws-iam-authenticator version

#=============================================================================#
#    Install the session-manager plugin
#=============================================================================#
RUN set -eux; \
  curl -fsSLo session-manager-plugin.deb \
  "https://s3.amazonaws.com/session-manager-downloads/plugin/latest/ubuntu_64bit/session-manager-plugin.deb"; \
  dpkg -i session-manager-plugin.deb; \
  session-manager-plugin --version


WORKDIR /opt/qh

ENTRYPOINT []

# http://label-schema.org/rc1/
LABEL org.label-schema.name="tg-aws" \
      org.label-schema.description="Terragrunt, Terraform, AWS tools" \
      org.label-schema.vendor="Fincore" \
      org.label-schema.schema-version="1.0"

# vim: ts=2 sw=2 et sts=2 ft=dockerfile
